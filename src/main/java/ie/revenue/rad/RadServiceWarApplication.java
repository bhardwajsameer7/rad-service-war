package ie.revenue.rad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RadServiceWarApplication {

	public static void main(String[] args) {
		SpringApplication.run(RadServiceWarApplication.class, args);
	}

}
