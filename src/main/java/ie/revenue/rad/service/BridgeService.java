package ie.revenue.rad.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import ie.revenue.rad.model.dtos.BridgeDTO;
import ie.revenue.rad.model.dtos.QueueDTO;
import ie.revenue.rad.model.mappers.BridgeMapper;
import ie.revenue.rad.model.mappers.QueueMapper;
import ie.revenue.rad.persistence.BridgeRepository;
import ie.revenue.rad.persistence.QueueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class BridgeService {

    public BridgeRepository bridgeRepository;

    @Autowired
    public BridgeService(BridgeRepository bridgeRepository){
        this.bridgeRepository = bridgeRepository;
    }

    public List<BridgeDTO> getBridges(String environment) throws JsonProcessingException {
        return BridgeMapper.entityToDTO(bridgeRepository.findAllBridges(environment));
    }

    public List<BridgeDTO> getBridgeByJndi(String environment , String jndi) throws JsonProcessingException {
        return BridgeMapper.entityToDTO(bridgeRepository.findBridgeByJndi(environment,jndi));
    }

}
