package ie.revenue.rad.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import ie.revenue.rad.model.dtos.EnvironmentDTO;
import ie.revenue.rad.model.entities.Environment;
import ie.revenue.rad.model.mappers.EnvironmentMapper;
import ie.revenue.rad.persistence.EnvironmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EnvironmentService {

    public EnvironmentRepository environmentRepository;

    @Autowired
    public EnvironmentService(EnvironmentRepository environmentRepository){
        this.environmentRepository = environmentRepository;
    }

    public List<EnvironmentDTO> getAllEnvironments() throws JsonProcessingException {
        return EnvironmentMapper.entityToDTO(environmentRepository.findAll());
    }

    public EnvironmentDTO getEnvironmentById(String environmentName) throws JsonProcessingException {

        Environment environmentObject = environmentRepository.findById(environmentName).get();

        ArrayList<Environment> enviromentList = new ArrayList<Environment>();
        enviromentList.add(environmentObject);

        return EnvironmentMapper.entityToDTO(enviromentList).get(0);
    }

    public void insertOrUpdateEnvironment(List<EnvironmentDTO> environmentDTOList) throws JsonProcessingException, NoSuchAlgorithmException {
        environmentRepository.saveAll(EnvironmentMapper.dtoToEntity(environmentDTOList));
    }

    public void deleteEnvironmentById(String environmentName) throws JsonProcessingException, NoSuchAlgorithmException {
          environmentRepository.deleteById(environmentName);
    }

}