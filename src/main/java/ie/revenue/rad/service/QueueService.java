package ie.revenue.rad.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import ie.revenue.rad.model.mappers.QueueMapper;
import ie.revenue.rad.model.dtos.QueueDTO;
import ie.revenue.rad.persistence.QueueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class QueueService {

    public QueueRepository queueRepository;

    @Autowired
    public QueueService(QueueRepository queueRepository){
        this.queueRepository = queueRepository;
    }

    public List<QueueDTO> getQueues(String environment) throws JsonProcessingException {
        return QueueMapper.entityToDTO(queueRepository.findAllQueues(environment));
    }

    public List<QueueDTO> getQueueById(String environment , String id) throws JsonProcessingException {
        return QueueMapper.entityToDTO(queueRepository.findQueueById(environment,id));
    }

    public List<QueueDTO> getQueueByJndi(String environment , String jndi) throws JsonProcessingException {
        return QueueMapper.entityToDTO(queueRepository.findQueueByJndi(environment,jndi));
    }

    public void insertOrUpdate(String environment, List<QueueDTO> queueList) throws JsonProcessingException, NoSuchAlgorithmException {
        queueRepository.saveAll(QueueMapper.dtoToEntity(queueList));
    }


}
