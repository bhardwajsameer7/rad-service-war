package ie.revenue.rad.service;

import ie.revenue.rad.model.entities.Server;
import ie.revenue.rad.persistence.QueueRepository;
import ie.revenue.rad.persistence.ServerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServerService {

    public ServerRepository serverRepository;

    @Autowired
    public ServerService(ServerRepository serverRepository){
        this.serverRepository = serverRepository;
    }

    public List<Server> getServers(){
        return serverRepository.findAll();
    }

}
