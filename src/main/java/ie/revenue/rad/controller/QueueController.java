package ie.revenue.rad.controller;

import ie.revenue.rad.common.RequestResponseMessage;
import ie.revenue.rad.model.dtos.QueueDTO;
import ie.revenue.rad.service.QueueService;
import ie.revenue.rad.utils.EnvironmentUtils;
import ie.revenue.rad.utils.QueueUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/environments")
public class QueueController {


    public QueueService queueService;

    @Autowired
    public QueueController(QueueService queueService){
        this.queueService = queueService;
    }

    @GetMapping("/{environment}/queues")
    public ResponseEntity<?> getQueues(@PathVariable("environment") String environment) throws Exception {
         return ResponseEntity.ok().body(queueService.getQueues(environment));
    }


     @GetMapping("/{environment}/queues/id/{id}")
    public ResponseEntity<?> getQueuebyId(@PathVariable("environment") String environment, @PathVariable("id") String id) throws Exception {
         return ResponseEntity.ok().body(queueService.getQueueById(environment,id));
    }


    @GetMapping("/{environment}/queues/jndi/{jndi}")
    public ResponseEntity<?> getQueueByJndi(@PathVariable("environment") String environment, @PathVariable("jndi") String asterisk_jndi) throws Exception {

        String jndi = asterisk_jndi.replace('*','/');
        return ResponseEntity.ok().body(queueService.getQueueByJndi(environment,jndi));

    }


    @PostMapping("/{environment}/queues")
    public ResponseEntity<?> insertOrUpdateQueues(@PathVariable("environment") String environment , @RequestBody List<QueueDTO> queueList) throws Exception {

            String normalised_environment = EnvironmentUtils.normalizeEnvironment(environment);
            queueService.insertOrUpdate(normalised_environment,queueList);
            return ResponseEntity.ok().body(RequestResponseMessage.OK);
    }

}
