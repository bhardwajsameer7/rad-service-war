package ie.revenue.rad.controller;

import ie.revenue.rad.service.ServerService;
import ie.revenue.rad.utils.EnvironmentUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/environments/all")
public class ServerController {

    public ServerService serverService;

    @Autowired
    public ServerController(ServerService serverService){
        this.serverService=serverService;
    }

    @GetMapping("/servers")
    public ResponseEntity<?> getServers() throws Exception {
            return ResponseEntity.ok().body(serverService.getServers());
    }

}
