package ie.revenue.rad.controller;

import ie.revenue.rad.common.RequestResponseMessage;
import ie.revenue.rad.model.dtos.EnvironmentDTO;
import ie.revenue.rad.service.EnvironmentService;
import ie.revenue.rad.utils.EnvironmentUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api")
public class EnvironmentController {

    public EnvironmentService environmentService;

    @Autowired
    public EnvironmentController(EnvironmentService environmentService){
        this.environmentService=environmentService;
    }

    @GetMapping("/environments")
    public ResponseEntity<?> getEnvironments() throws Exception {
        return ResponseEntity.ok().body(environmentService.getAllEnvironments());
    }

    @GetMapping("/environments/{environment}")
    public ResponseEntity<?> getEnvironments(@PathVariable("environment") String environment) throws Exception {
       return ResponseEntity.ok().body(environmentService.getEnvironmentById(environment));
     }

    @PostMapping("/admin/environments")
    public ResponseEntity<?> insertOrUpdateEnvironment(@RequestBody List<EnvironmentDTO> environmentDTOList) throws Exception {
            environmentService.insertOrUpdateEnvironment(environmentDTOList);
            return ResponseEntity.ok().body(RequestResponseMessage.OK);
    }

    @DeleteMapping("/admin/environments/{environment}")
    public ResponseEntity<?> deleteEnvironment(@PathVariable("environment") String environment) throws Exception {
            environmentService.deleteEnvironmentById(environment);
            return ResponseEntity.ok().body(RequestResponseMessage.OK);
    }
}