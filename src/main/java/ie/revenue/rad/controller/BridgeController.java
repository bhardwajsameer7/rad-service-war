package ie.revenue.rad.controller;

import ie.revenue.rad.service.BridgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/environments")
public class BridgeController {


    public BridgeService bridgeService;

    @Autowired
    public BridgeController(BridgeService bridgeService){
        this.bridgeService = bridgeService;
    }

    @GetMapping("/{environment}/bridges")
    public ResponseEntity<?> getBridges(@PathVariable("environment") String environment) throws Exception {
         return ResponseEntity.ok().body(bridgeService.getBridges(environment));
    }

    @GetMapping("/{environment}/bridges/jndi/{jndi}")
    public ResponseEntity<?> getBridgeByJndi(@PathVariable("environment") String environment, @PathVariable("jndi") String asterisk_jndi) throws Exception {

        String jndi = asterisk_jndi.replace('*','/');
        return ResponseEntity.ok().body(bridgeService.getBridgeByJndi(environment,jndi));

    }

}
