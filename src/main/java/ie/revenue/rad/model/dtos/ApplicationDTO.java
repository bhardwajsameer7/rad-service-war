package ie.revenue.rad.model.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ApplicationDTO {

    private String environment;
    private String server;
    private String name;
    private String version;
    private String type;
    private List<String> databases;
    private List<String> endpoints;
    private List<List<String>> queues;

}
