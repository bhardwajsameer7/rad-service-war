package ie.revenue.rad.model.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class QueueDTO {

    private String id;
    private String environment;
    private String server;
    private String name;
    private String jndi;

}
