package ie.revenue.rad.model.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class EnvironmentDTO {

    private String name;
    private boolean active;
}
