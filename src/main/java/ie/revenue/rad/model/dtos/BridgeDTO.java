package ie.revenue.rad.model.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BridgeDTO {

    private String id;
    private String environment;
    private String server;
    private String name;
    private String sourceName;
    private String sourceJndi;
    private String sourceServer;
    private String targetName;
    private String targetJndi;
    private String targetServer;

}
