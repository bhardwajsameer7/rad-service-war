package ie.revenue.rad.model.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name="ENVIRONMENTS")
public class Environment {

    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "active")
    private boolean active;
}
