package ie.revenue.rad.model.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name="QUEUES")
public class Queue {

    @Column(name = "id")
    @Id
    private String id;

    @Column(name = "data" , columnDefinition = "jsonb")
    private String data;

}
