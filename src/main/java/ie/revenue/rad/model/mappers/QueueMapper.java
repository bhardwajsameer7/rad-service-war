package ie.revenue.rad.model.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ie.revenue.rad.helper.MD5Helper;
import ie.revenue.rad.model.entities.Queue;
import ie.revenue.rad.model.dtos.QueueDTO;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class QueueMapper {

    public static List<QueueDTO> entityToDTO(List<Queue> queues) throws JsonProcessingException {

        List<QueueDTO> queueDTOList = new ArrayList<QueueDTO>();

        for (Queue queue: queues) {
            ObjectMapper objectMapper = new ObjectMapper();
            queueDTOList.add(objectMapper.readValue(queue.getData(),QueueDTO.class));
        }
        return queueDTOList;
    }

    public static List<Queue> dtoToEntity(List<QueueDTO> queueDTOList) throws JsonProcessingException, NoSuchAlgorithmException {

        List<Queue> queueList = new ArrayList<Queue>();

        for(QueueDTO queueDto: queueDTOList){

            Queue queue = new Queue();
            ObjectMapper objectMapper = new ObjectMapper();

            queue.setId(MD5Helper.normalStringToMd5String(queueDto.getEnvironment()+queueDto.getJndi()));
            queue.setData(objectMapper.writeValueAsString(queueDto));

            queueList.add(queue);
        }

        return queueList;
    }
}
