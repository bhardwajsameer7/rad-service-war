package ie.revenue.rad.model.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ie.revenue.rad.helper.MD5Helper;
import ie.revenue.rad.model.dtos.BridgeDTO;
import ie.revenue.rad.model.entities.Bridge;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class BridgeMapper {

    public static List<BridgeDTO> entityToDTO(List<Bridge> bridges) throws JsonProcessingException {

        List<BridgeDTO> bridgeDTOList = new ArrayList<BridgeDTO>();

        for (Bridge bridge: bridges) {
            ObjectMapper objectMapper = new ObjectMapper();
            bridgeDTOList.add(objectMapper.readValue(bridge.getData(),BridgeDTO.class));
        }
        return bridgeDTOList;
    }

    public static List<Bridge> dtoToEntity(List<BridgeDTO> bridgeDTOList) throws JsonProcessingException, NoSuchAlgorithmException {

        List<Bridge> bridgeList = new ArrayList<Bridge>();

        for(BridgeDTO bridgeDto: bridgeDTOList){

            Bridge bridge = new Bridge();
            ObjectMapper objectMapper = new ObjectMapper();

            bridge.setId(MD5Helper.normalStringToMd5String(bridgeDto.getEnvironment()+bridgeDto.getName()));
            bridge.setData(objectMapper.writeValueAsString(bridgeDto));

            bridgeList.add(bridge);
        }

        return bridgeList;
    }
}
