package ie.revenue.rad.model.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ie.revenue.rad.helper.MD5Helper;
import ie.revenue.rad.model.dtos.EnvironmentDTO;
import ie.revenue.rad.model.dtos.QueueDTO;
import ie.revenue.rad.model.entities.Environment;
import ie.revenue.rad.model.entities.Queue;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class EnvironmentMapper {

    public static List<EnvironmentDTO> entityToDTO(List<Environment> environments) throws JsonProcessingException {

        List<EnvironmentDTO> environmentDTOList = new ArrayList<EnvironmentDTO>();

        for(Environment environment : environments){

            EnvironmentDTO dto = new EnvironmentDTO();

            dto.setName(environment.getName());
            dto.setActive(environment.isActive());

            environmentDTOList.add(dto);
        }

        return environmentDTOList;
    }

    public static List<Environment> dtoToEntity(List<EnvironmentDTO> environmentDTOs) throws JsonProcessingException, NoSuchAlgorithmException {

        List<Environment> environmentList = new ArrayList<Environment>();

        for(EnvironmentDTO environmentDTO : environmentDTOs){

            Environment environmentObject = new Environment();

            environmentObject.setName(environmentDTO.getName());
            environmentObject.setActive(environmentDTO.isActive());

            environmentList.add(environmentObject);

        }

        return environmentList;
    }
}
