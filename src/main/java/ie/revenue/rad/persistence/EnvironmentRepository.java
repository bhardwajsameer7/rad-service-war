package ie.revenue.rad.persistence;

import ie.revenue.rad.model.entities.Environment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnvironmentRepository extends JpaRepository<Environment,String> {

}


