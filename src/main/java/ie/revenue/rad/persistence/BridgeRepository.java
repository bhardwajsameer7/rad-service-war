package ie.revenue.rad.persistence;

import ie.revenue.rad.model.entities.Bridge;
import ie.revenue.rad.model.entities.Queue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BridgeRepository extends JpaRepository<Bridge,String> {

    @Query(value = "select * from BRIDGES where data ->> 'environment' = :environment",nativeQuery = true)
    List<Bridge> findAllBridges(@Param("environment") String environment);

    @Query(value = "select * from BRIDGES where data ->> 'environment' = :environment and (data ->> 'sourceJndi' = :jndi or data ->> 'targetJndi' = :jndi)",nativeQuery = true)
    List<Bridge> findBridgeByJndi(@Param("environment") String environment, @Param("jndi") String jndi );

}


