package ie.revenue.rad.persistence;

import ie.revenue.rad.model.entities.Queue;
import ie.revenue.rad.model.entities.Server;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServerRepository extends JpaRepository<Server,String> {

}


