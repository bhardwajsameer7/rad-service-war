package ie.revenue.rad.persistence;

import ie.revenue.rad.model.entities.Queue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QueueRepository extends JpaRepository<Queue,String> {

    @Query(value = "select * from QUEUES where data ->> 'environment' = :environment",nativeQuery = true)
    List<Queue> findAllQueues(@Param("environment") String environment);

    @Query(value = "select * from QUEUES where data ->> 'environment' = :environment and data ->> 'id' = :id" ,nativeQuery = true)
    List<Queue> findQueueById(@Param("environment") String environment, @Param("id") String id );


    @Query(value = "select * from QUEUES where data ->> 'environment' = :environment and data ->> 'jndi' = :jndi" ,nativeQuery = true)
    List<Queue> findQueueByJndi(@Param("environment") String environment, @Param("jndi") String jndi );

}


