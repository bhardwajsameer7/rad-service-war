package ie.revenue.rad.utils;

import ie.revenue.rad.model.dtos.EnvironmentDTO;

public class EnvironmentUtils {

    public static boolean isValidEnvironment(String environment){

        boolean flag = false;

        if(environment == null || environment.isBlank() || environment.isEmpty()){ return flag; } //check null, empty and blank.

        if(environment.length() != 4) { return flag; } // length has to be 4.

        char first = environment.charAt(0);
        char second = environment.charAt(1);
        char third = environment.charAt(2);
        int fourth = Integer.parseInt(String.valueOf(environment.charAt(3)));

        if(first == 'i' && second == 'n' && third =='t' && (fourth >= 1 && fourth <=9) ){ return !flag; } // should be int1 to int9
        if(first == 's' && second == 'y' && third =='y' && (fourth >= 1 && fourth <=9) ){ return !flag; } //should be sys1 to sys9

        return flag;
    }

    public static String normalizeEnvironment(String environment){
        return environment.trim().toLowerCase();
    }

    public static boolean checkValidEnvironmentInput(Object environmentInput){
        return (environmentInput != null) ? true : false;
    }
}
