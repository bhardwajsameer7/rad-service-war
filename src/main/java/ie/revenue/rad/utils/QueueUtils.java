package ie.revenue.rad.utils;

public class QueueUtils {

    public static boolean isValidJNDI(String jndi){

        boolean flag = false;

        if(jndi == null || jndi.isEmpty() || jndi.isBlank()){ return flag;}

        char first = jndi.charAt(0);
        char second = jndi.charAt(1);
        char last = jndi.charAt(jndi.length() - 1);

        if( first == '/' || last == '/'){
            return flag;
        }

        if(first != 'i' || second != 'e'){
            return flag;
        }

        return !flag;
    }
}
