package ie.revenue.utils.rad;

import ie.revenue.rad.utils.EnvironmentUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnvironmentUtilsTest {

    @Test
    public void checkValidEnvironment(){

        String test1 = "int1";
        String test2 = "sys9";
        String test3 = "int10";
        String test4 = "";
        String test5 = "   ";
        String test6 = null;
        String test7 = "iyt2";
        String test8 = "yyss4";
        String test9 = "INT1";

        assertTrue(EnvironmentUtils.isValidEnvironment(test1));
        assertFalse(EnvironmentUtils.isValidEnvironment(test2));
        assertFalse(EnvironmentUtils.isValidEnvironment(test3));
        assertFalse(EnvironmentUtils.isValidEnvironment(test4));
        assertFalse(EnvironmentUtils.isValidEnvironment(test5));
        assertFalse(EnvironmentUtils.isValidEnvironment(test6));
        assertFalse(EnvironmentUtils.isValidEnvironment(test7));
        assertFalse(EnvironmentUtils.isValidEnvironment(test8));
        assertFalse(EnvironmentUtils.isValidEnvironment(test9));

    }
}
